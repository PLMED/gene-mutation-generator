
import re
from openpyxl import load_workbook
from docx.shared import Pt, Inches
from docx import Document

mutation_list = []

#starting parse

class Mutation:
  def __init__(self, status, gene, cdna_change, prot_change, allelefreq, cosmic_id, dbsnip_id, transcript, genome, chrom, start, refvar):
    self.status = status
    self.gene = gene
    self.cdna_change = cdna_change
    self.prot_change = prot_change
    self.allelefreq = allelefreq
    self.cosmic_id = cosmic_id
    self.dbsnip_id = dbsnip_id
    self.transcript = transcript
    self.genome = genome
    self.chrom = chrom
    self.start = start
    self.refvar = refvar

  def myfunc(self):
      print("Hello my name is " + self.name)


def convert_workbook(excel_path: str, word_path: str):
    wb = load_workbook(excel_path, data_only=True)
    sh = wb.active

    columnMap = {}

    hex_yellow = "FFFFFF00" #openpyxl prepends FF to the HEX color
    hex_green = "FF92D050"

    yellow_group = []
    green_group = []

    MG_name = None

    #walk through the column headers
    for i in range(1, sh.max_column):
        cell_obj = sh.cell(row=1, column=i)
        #create a column name to position map
        #Only use first column name position, for VEP_RefSeq_gene alone there are 15 duplicate column names
        if cell_obj.value not in columnMap:
            columnMap[cell_obj.value] = i
        #if cell_obj.value == "VEP_RefSeq_gene":
        #    print(cell_obj.value + " i:" + str(i))
        # Determine the MG_name based on column name
        m = re.search('MG[1-2][0-9]-[0-9]+_', cell_obj.value)
        if (m is not None):
                if MG_name is None:
                    MG_name = m.string.split("_")[0]
                else:
                    #Check each instance of MG to make sure that they match
                    if MG_name != m.string.split("_")[0]:
                        print("MG name mismatch: current = [" + MG_name + "] new = [" + m.string.split("_")[0] + "]")

    #Determine the rows that are either yellow or green
    #Yellow, status = 0, Green, status = 1
    row_count = sh.max_row
    for i in range(2, sh.max_row):
        if not sh.row_dimensions[i].hidden:
            cell = "A" + str(i)
            status = -1
            if sh[cell].fill.start_color.index == hex_yellow:
                status = 0
                yellow_group.append(i)
            elif sh[cell].fill.start_color.index == hex_green:
                status = 1

            if status > -1:
                #Cell Value: NRAS-cBio-Portal
                gene = str(sh.cell(row=i, column=columnMap["VEP_RefSeq_gene"]).value).split("-")[0]
                #Cell Value: NM_002524.4:c.35G>A
                cdna_change = str(sh.cell(row=i, column=columnMap["HGVS_RefSeq_cdna_change"]).value).split(":")[1]
                #Cell Value: NP_002515.1:p.Gly12Asp
                prot_change = str(sh.cell(row=i, column=columnMap["HGVS_RefSeq_prot_change"]).value).split(":")[1]

                #mutation = cdna_change + "; " + prot_change
                #0.418
                allelefreq = sh.cell(row=i, column=columnMap[MG_name + "_VAF"]).value

                cosmic_id = str(sh.cell(row=i, column=columnMap["COSMIC_ID"]).value)
                dbsnip_id = str(sh.cell(row=i, column=columnMap["ID"]).value)

                id = cosmic_id + "; " + dbsnip_id

                transcript = str(sh.cell(row=i, column=columnMap["VEP_RefSeq_transcript"]).value)
                genome = "Hg19"
                chrom = str(sh.cell(row=i, column=columnMap["#CHROM"]).value)
                pos_start = str(sh.cell(row=i, column=columnMap["POS"]).value)
                #c.38G>A
                # refvar = re.sub('c\.(\d+)', '', cdna_change)
                refvar = re.sub('c\.([^a-zA-Z]+)', '', cdna_change)

                mutation_list.append(Mutation(status,gene,cdna_change,prot_change,allelefreq,cosmic_id,dbsnip_id,transcript,genome,chrom,pos_start,refvar))

                '''
                print("Status: " + str(status))
                print("Gene: " + gene)
                print("Mutation: " + mutation)
                print("Allele Frequency (%): " + "%.1f%%" % (allelefreq * 100))
                print("ID: " + id)
                print("Transcript: " + transcript)
                print("Genome: " + genome)
                print("Chrom: " + chrom)
                print("Start: " + pos_start)
                print("RefVar: " + refvar)
                print("\n")
                '''
    #ordered group will be used to construct reports
    ordered_group = []
    yellow_group = []
    green_group = []

    for mutation in mutation_list:
        #if yellow add to the first group
        if mutation.status == 0:
            if mutation.gene not in yellow_group:
                yellow_group.append(mutation.gene)

        #if green add to the second group
        if mutation.status == 1:
            if mutation.gene not in green_group:
                green_group.append(mutation.gene)

    #remove gene from green group that are in yellow group
    for gene in yellow_group:
        if gene in green_group:
            green_group.remove(gene)

    #first add yellow group
    for gene in yellow_group:
        ordered_group.append(gene)

    #next add green group
    for gene in green_group:
        ordered_group.append(gene)


    #Start report generation

    document = Document()
    docfont = document.styles['Normal'].font
    docfont.name = 'Times New Roman'

    summary_text = 'The following () have been detected in this bone marrow specimen'
    summary = document.add_paragraph()
    summary_run = summary.add_run(summary_text)
    summary_run_font = summary_run.font
    summary_run_font.bold = True
    summary_run_font.name = 'Times New Roman'
    summary_run_font.size = Pt(12)

    #color of text box
    #shading_elm_1 = parse_xml(r'<w:shd {} w:fill="F7F7F7"/>'.format(nsdecls('w')))

    header = document.add_paragraph()

    for gene in ordered_group:
        header.add_run("Gene: {}".format(gene)).bold = True
        header.add_run().add_break()

        for mutation in mutation_list:
            if(mutation.gene == gene):
                header.add_run("Mutation: {}; {}".format(mutation.cdna_change, mutation.prot_change)).bold = True
                header.add_run().add_break()
                header.add_run("Allele Frequency (%): {0:0.1f}%".format(mutation.allelefreq * 100)).bold = True
                header.add_run().add_break()
                #-; .
                if(mutation.cosmic_id != "-") and (mutation.dbsnip_id != "."):
                    header.add_run("ID: {};{}".format(mutation.cosmic_id, mutation.dbsnip_id)).bold = True
                    header.add_run().add_break()
                elif (mutation.cosmic_id == "-") and (mutation.dbsnip_id != "."):
                    header.add_run("ID: {}".format(mutation.dbsnip_id)).bold = True
                    header.add_run().add_break()
                elif (mutation.cosmic_id != "-") and (mutation.dbsnip_id == "."):
                    header.add_run("ID: {}".format(mutation.cosmic_id)).bold = True
                    header.add_run().add_break()

        header.add_run().add_break()
        header.add_run('Interpretation: ').bold = True
        header.add_run().add_break()
        header.add_run().add_break()
        '''
        table = document.add_table(rows=1, cols=1)
        hdr_cells = table.rows[0].cells
        hdr_cells[0].text = 'Interpretation: '
        '''

        #table.rows[0].cells[0]._tc.get_or_add_tcPr().append(shading_elm_1)

        #footer = document.add_paragraph()
        #footer.add_run().add_break()


    panel_text = 'The following 97 genes were tested on this panel and with the exception of the genes listed above; none of these contain any clinically significant mutations'
    document.add_paragraph().add_run(panel_text).bold = True

    geneList = (
    ('ABL1\n', 'BTK', 'CRLF2', 'FOXO1', 'JAK1', 'MEF2B', 'PHF6', 'RUNX1', 'TCF3' ),
    ('AKT1\n', 'CALR', 'CSF1R', 'FOXP1', 'JAK2', 'MPL', 'PIAS2', 'SETBP1', 'TET2' ),
    ('ARNTL\n', 'CARD11', 'CSF3R', 'GATA1', 'JAK3', 'MYBL2', 'PIK3R2', 'SF3B1', 'TNFAIP3' ),
    ('ASXL1\n', 'CBL', 'CUX1', 'GATA2', 'KDM6A', 'MYD88', 'PLCG2', 'SH2B3', 'TNFRSF14' ),
    ('ATM\n', 'CBLB', 'DNMT3A', 'GNA13', 'KIF17', 'NF1', 'PRDM1', 'SMC1A', 'TP53' ),
    ('BCL2\n', 'CCND1', 'EP300', 'HNRNPK', 'KIT', 'NOTCH1', 'PRMT5', 'SMC3', 'U2AF1' ),
    ('BCL6\n', 'CD79B', 'ETV6', 'HRAS', 'KLHL6', 'NOTCH2', 'PTEN', 'SOCS1', 'WHSC1' ),
    ('BCOR\n', 'CDKN2A', 'EZH2', 'IDH1', 'KMT2A', 'NOTCH3', 'PTPN11', 'SRSF2', 'WT1' ),
    ('BCORL1\n', 'CEBPA', 'FAM5C', 'IDH2', 'KMT2C', 'NPM1', 'PTPRD', 'STAG2', 'ZRSR2' ),
    ('BIRC3\n', 'CLSTN1', 'FBXW7', 'IKZF1', 'KMT2D', 'NRAS', 'PTPRT', 'STAT3', '' ),
    ('BRAF\n', 'CREBBP', 'FLT3', 'IL7R', 'KRAS', 'PAX5', 'RAD21', 'SUZ12', '' ),
    )


    table = document.add_table(rows=len(geneList), cols=len(geneList[0]))
    table.style = 'Table Grid'
    table.allow_autofit = False
    i = 0
    ii = 0
    for row in geneList:

        for column in row:
            table.rows[i].cells[ii].text = column
            ii = ii + 1
        i = i + 1
        ii = 0

    for row in table.rows:
        for cell in row.cells:
            paragraphs = cell.paragraphs
            for paragraph in paragraphs:
                for run in paragraph.runs:
                    font = run.font
                    font.name = 'Times New Roman'
                    font.size = Pt(9)


    panel_text = 'Additional Details on Mutation Identified:'
    document.add_paragraph().add_run().add_break()
    document.add_paragraph().add_run(panel_text).bold = True

    mtable = document.add_table(rows=len(mutation_list) + 1, cols=7)
    mtable.style = 'Table Grid'

    mtable.rows[0].cells[0].text = "Gene"
    mtable.rows[0].cells[1].text = "Transcript"
    mtable.rows[0].cells[2].text = "Genome"
    mtable.rows[0].cells[3].text = "Chrom"
    mtable.rows[0].cells[4].text = "Start"
    mtable.rows[0].cells[5].text = "End"
    mtable.rows[0].cells[6].text = "RefVar"

    for i in range(0, 7):
        mtable.rows[0].cells[i].paragraphs[0].runs[0].font.bold = True

    i = 1
    for mutation in mutation_list:
        mtable.rows[i].cells[0].text = mutation.gene
        mtable.rows[i].cells[1].text = mutation.transcript
        mtable.rows[i].cells[2].text = mutation.genome
        mtable.rows[i].cells[3].text = mutation.chrom
        mtable.rows[i].cells[4].text = mutation.start
        mtable.rows[i].cells[5].text = ""
        mtable.rows[i].cells[6].text = mutation.refvar
        i = i +1

    for row in mtable.rows:
        for cell in row.cells:
            paragraphs = cell.paragraphs
            for paragraph in paragraphs:
                for run in paragraph.runs:
                    font = run.font
                    font.name = 'Times New Roman'
                    font.size = Pt(9)

    #Methodology Section
    meth = document.add_paragraph()
    meth.add_run().add_break()

    meth_title = 'Methodology'
    meth.add_run(meth_title).bold = True
    meth.add_run().add_break()
    meth.add_run().add_break()

    meth0 = 'DNA is extracted and hybridized with custom-designed probes to enrich the targeted regions of 97 genes associated with hematologic malignancies. Samples are then sequenced on the Illumina HiSeq 2500 (Illumina, Inc, CA). A custom bioinformatics pipeline aligns the data to human reference genome GRCh37 to call variants. The limit of detection (related in part to depth of coverage, neoplastic cell percentage, and allelic frequency for the mutation) was determined to be 5% allele frequency, at which our assay has sensitivity of 98% and 91%, respectively, to detect single nucleotide variants (SNVs) and insertions/deletions (indels). Mutant allele populations below this detection limit will not be reliably detected by this method. Pseudogenes, highly homologous regions, and repeat regions may interfere with the detection of variants in this assay. This assay targets genes involved in hematologic malignancies. Some of the genes targeted may also cause inherited genetic disorders, variants in these genes will not be reported unless they are determined to contribute to the diagnosis, prognosis, or treatment of hematologic malignancies.'
    meth.add_run(meth0)
    meth.add_run().add_break()
    meth.add_run().add_break()

    meth1 = 'In addition to the mutations/variants reported above, several benign polymorphisms and variants of unknown/uncertain clinical significance have been detected. These variants can be made available upon request'
    meth.add_run(meth1)
    meth.add_run().add_break()
    meth.add_run().add_break()
    meth.add_run().add_break()

    meth2 = 'This test was developed and its performance characteristics determined by the Clinical Molecular and Genomic Pathology Laboratory at the University of Kentucky. It has not been cleared or approved by the U.S. Food and Drug Administration. This test, which utilizes analyte specific reagents, does not require FDA approval. This test is used for clinical purposes. It should not be regarded as investigational or for research. This laboratory is certified under the Clinical Laboratory Improvement Amendments of 1988 (CLIA-88) as qualified to perform high complexity clinical laboratory testing.'
    meth.add_run(meth2)
    meth.add_run().add_break()
    meth.add_run().add_break()

    meth3 = 'This service has been rendered in part by a resident. A pathologist has personally reviewed the test results and has rendered and is responsible for the diagnosis that appears on the report.'
    meth.add_run(meth3)
    meth.add_run().add_break()

    sections = document.sections
    for section in sections:
        section.top_margin = Inches(1)
        section.bottom_margin = Inches(1)
        section.left_margin = Inches(1)
        section.right_margin = Inches(1)

    document.save(word_path)


    #parsing blocks

    '''from docx.text.run import Run, _Text
    from docx.text.paragraph import Paragraph
    from docx.table import Table, _Cell
    from docx.document import Document as _Document
    from docx.oxml.text.paragraph import CT_P
    from docx.oxml.text.run import CT_R, CT_Text
    from docx.oxml.table import CT_Tbl

    def iter_block_items(parent):
        if isinstance(parent, _Document):
            parent_elm = parent.element.body
            # print(parent_elm.xml)
        elif isinstance(parent, _Cell):
            parent_elm = parent._tc
        else:
            raise ValueError("something's not right")
    
        for child in parent_elm.iterchildren():
            if isinstance(child, CT_P):
                for p_child in child.iterchildren():
                    if isinstance(p_child, CT_R):
                        for r_child in p_child.iterchildren():
                            if r_child.text is not None:
                                yield r_child.text
            elif isinstance(child, CT_Tbl):
                yield Table(child, parent)
    
    def table_print(block):
        table=block
        for row in table.rows:
            for cell in row.cells:
                for paragraph in cell.paragraphs:
                    print(paragraph.text,'  ',end='')
                    #y.write(paragraph.text)
                    #y.write('  ')
            print("\n")
            #y.write("\n")
    
    for block in iter_block_items(document):
        if isinstance(block, Paragraph):
            print("Paragraph [" + block.text + "]")
        elif isinstance(block, Run):
            print(block.text)
            print("Run [{}]".format(block.text))
        elif isinstance(block, Table):
            text = block.cell(0,0)
            print("Table [" + text.text + "]")
            #table_print("Table [" + text.text + "]")
        elif isinstance(block, str):
            print("Text: {}".format(block))'''
