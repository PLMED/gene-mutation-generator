import docx
import openpyxl
import tkinter
import functools
import PySimpleGUI as sg
from generator.Launcher import convert_workbook

single = [
    [
        sg.Text('Convert a single file')
    ], [
        sg.Text('File to convert', size=(15, 1)),
        sg.Input(key="SINGLECONVERTINPUT"),
        sg.FileBrowse(file_types=(("Excel Files", "*.xlsx"),))
    ], [
        sg.Text('File to save as', size=(15, 1)),
        sg.Input(key="SINGLECONVERTOUTPUT"),
        sg.FileSaveAs(file_types=(("Word Files", "*.docx"),))
    ]
]

folder = [
    [
        sg.Text('Convert an entire folder')
    ], [
        sg.Text('Folder with files', size=(17, 1)),
        sg.Input(key="FOLDERCONVERTINPUT"),
        sg.FolderBrowse()
    ], [
        sg.Text('Folder to save in', size=(17, 1)),
        sg.Input(key="FOLDERCONVERTOUTPUT"),
        sg.FolderBrowse()
    ]
]

layout = [
    [
        sg.Text('', size=(53,1)),
        sg.Button("Switch to Single File Mode", key="SINGLEBTN", visible=False),
        sg.Button("Switch to Entire Folder Mode", key="FOLDERBTN", visible=False)
    ], [
        sg.Column(single, key="SINGLE", visible=True),
        sg.Column(folder, key="FOLDER", visible=False)
    ], [
        sg.Button("Start File Conversion", key="SINGLECONVERT", visible=True),
        sg.Button("Start Folder Conversion", key="FOLDERCONVERT", visible=False)
    ]
]

window = sg.Window('Gene Mutation Report Generator', layout)

while True:
    event, values = window.read(timeout=250)
    if values is not None and values['SINGLECONVERTOUTPUT'] != '' and not values['SINGLECONVERTOUTPUT'].endswith(".docx"):
        window.Element('SINGLECONVERTOUTPUT').update(values['SINGLECONVERTOUTPUT'] + '.docx')
    if event is None or event == 'Exit':
        break
    if event == 'SINGLEBTN':
        window.Element('SINGLECONVERTINPUT').Update('')
        window.Element('SINGLECONVERTOUTPUT').Update('')
        window.Element('SINGLE').Update(visible=True)
        window.Element('SINGLECONVERT').Update(visible=True)
        window.Element('SINGLEBTN').Update(visible=False)
        window.Element('FOLDER').Update(visible=False)
        window.Element('FOLDERCONVERT').Update(visible=False)
        window.Element('FOLDERBTN').Update(visible=True)
    if event == 'FOLDERBTN':
        window.Element('FOLDERCONVERTINPUT').Update('')
        window.Element('FOLDERCONVERTOUTPUT').Update('')
        window.Element('FOLDER').Update(visible=True)
        window.Element('FOLDERCONVERT').Update(visible=True)
        window.Element('FOLDERBTN').Update(visible=False)
        window.Element('SINGLE').Update(visible=False)
        window.Element('SINGLECONVERT').Update(visible=False)
        window.Element('SINGLEBTN').Update(visible=True)
    if event == 'SINGLECONVERT':
        if values['SINGLECONVERTINPUT'] == '':
            sg.Popup("Error", "Please enter a file to convert")
        elif values['SINGLECONVERTOUTPUT'] == '':
            sg.Popup("Error", "Please enter a save file location")
        else:
            file_to_convert = values['SINGLECONVERTINPUT']
            file_to_save_as = values['SINGLECONVERTOUTPUT']
            if not file_to_save_as.endswith(".docx"):
                file_to_save_as += ".docx"
            window.Element('SINGLECONVERTINPUT').Update('')
            window.Element('SINGLECONVERTOUTPUT').Update('')

            # Todo: Add call to convert(file_to_convert, file_to_save_as)
            convert_workbook(file_to_convert, file_to_save_as)

            sg.Popup('File Conversion Results', "Converted {} and saved as {}".format(file_to_convert, file_to_save_as))
    if event == 'FOLDERCONVERT':
        if values['FOLDERCONVERTINPUT'] == '':
            sg.Popup("Error", "Please enter a folder to convert")
        elif values['FOLDERCONVERTOUTPUT'] == '':
            sg.Popup("Error", "Please enter a save file location")
        else:
            folder_to_convert = values['FOLDERCONVERTINPUT']
            folder_to_save_in = values['FOLDERCONVERTOUTPUT']
            window.Element('FOLDERCONVERTINPUT').Update('')
            window.Element('FOLDERCONVERTOUTPUT').Update('')

            # Todo: Add call to convert(folder_to_convert, folder_to_save_in)

            sg.Popup('Folder Conversion Results', "Converted all files in {} and saved in {}".format(folder_to_convert, folder_to_save_in))
    window.Refresh()
window.close()
